var MoveRate = 3;
var keys = {};
var attackx = 0;
var attacky = 0;
setInterval(loop, 20); //Multiple loops are needed here
setInterval (improvedShot, 20);
setInterval(mousePosition(), 20);//So that multiple actions can be done at same time;

$(document).keydown(function (e) {
    keys[e.which] = true;
});

$(document).keyup(function (e) {
    delete keys[e.which];
});


function move(){// defines character movement
    var pos = $('#p1').position();
    for (var i in keys){
        if (i == 38 || i == 87){ //up
            if (pos.top - 3 >= 0){
                $('#p1').css('top', pos.top -= MoveRate);
            }
        }
        if (i == 39 || i == 68){ //right
            if (pos.left - 6 <= $('#main_body').width()){
                $('#p1').css('left', pos.left += MoveRate);
            }
        }
        if (i == 37 || i == 65){ //left
            if (pos.left - 3  >= 0){
                $('#p1').css('left', pos.left -= MoveRate);
            }
        }
        if (i == 40 || i == 83){ //down
            if (pos.top + 23 <= $('#main_body').height()){
                $('#p1').css('top', pos.top += MoveRate);
            }
        }
    }
};
var currentMousePos = { x: -1, y: -1 };
function mousePosition(){
    $(document).mousemove(function(event) {
        currentMousePos.x = event.pageX;
        currentMousePos.y = event.pageY;
        var mousePos = "" + currentMousePos.x;
        mousePos +=", "
        mousePos += "" + currentMousePos.y;
        $('#pos').html(mousePos);
        $('#pos').css('left', $('#main_body').width() - 50);
        $('#pos').css('top', 50);
    });
}
function improvedShot(){
    for (var i in keys){
        var amount = $('.shot').length;
        var obj = $('#' + amount + '');
        var leftpos = currentMousePos.x - 12;
        var toppos = currentMousePos.y - 12;
        if (i == 32){
            $('#main_body').append("<div class='shot' id='" + amount + "' style='left: "+leftpos+"; top: "+toppos+";'></div>");
            travel(obj);
            amount++;
        }
    }
}
function travel(thing){
    //var start = $('#p1').position();
    //var end = $currentMousePos;
    //var xVec = end.x - start.left;
    //var yVec = end.y - start.top;
    while (isLegalHori(thing.position().left) && isLegalVert(thing.position().top)){
        setInterval(function(){
            thing.css('left', thing.position().left + 5);
            thing.css('top', thing.position().top + 5);
        }, 20);
    }

}
function isLegalVert(int){
    return (int > 0 && int < $('#main_body').height());
}
function isLegalHori(int){
    return (int > 0 && int < $('#main_body').width());
}
function shoot() {// defines bullet origin point
    var attackPos = $('#p1').offset();
    for (var i in keys){
        if (i == 32){ //spacebar
            $('#attack').css('left', attackPos.left +=0);
            $('#attack').css('top', attackPos.top -=15);
            attackx = $('#p1').offset().left;
            attacky = $('#p1').offset().top;

        }
    }
};
function getPosition(){
    var pos = $('#p1').position();
    var text = '<p>';
    text += pos.left;
    text += ', ';
    text += pos.top;
    text += '</p>';
    $('#map').html(text);
    $('#map').css('left', $('#main_body').width() - 50);
    $('#map').css('top', 15);
}

function updatePositions() {// defines movement of all non character objets (i.e. attacks, npcs, weather)
    $('#attack').css('top', attacky -=5);
};
/*function setPosition(sprite) {
 var e = document.getElementById(sprite.element);
 e.style.left = sprite.x + 'px';
 $('#sprite').css('top', pos.top += MoveRate);
 };

 function showSprites() {
 setPostition(p1);
 };*/

function loop() { // updates game data every 20 ms
    move();
    //shoot();
    //updatePositions();
    getPosition();
    //showSprites();
};